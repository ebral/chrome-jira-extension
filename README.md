# Chrome JIRA Extensions

This chrome extension adds some customisations to JIRA that make the UI easier to use.

# Customisations

## Background color by priority

By default, Jira displays the priority of a ticket with a small colored arrow. Because the arrow is so small, it's very hard to discern the actual priority.
This tweak colors the whole background of the tickets to the color of the priority.

**Configuration**
1. You can change the **columns** for which this tweak applies by opening the file `content.json` and editing the function `highlightColumns()`.
2. You can change the **colors** of the priorties by opening the file `content.json` and editing the function `getPriorityStyle()`.

