function highlightColumns() {
  highlightColumn('In progress', 'yellow');
  highlightColumn('In review', 'yellow');
}

function highlightColumn(columnName, defaultStyle) {
  var pool = document.querySelector('#ghx-pool');

  var columnHeaders = pool.querySelector('#ghx-column-headers');

  var columnIndex = Array.from(columnHeaders.children)
    .findIndex(child => child.querySelector('h2').innerHTML.toLowerCase() == columnName.toLowerCase());

  var swimlanes = pool.querySelectorAll('.ghx-swimlane');

  var columns = Array.from(swimlanes)
    .map(swimlane => swimlane.querySelector('.ghx-columns'))
    .map(columns => columns.children[columnIndex]);

  var issues = columns
    .filter(column => !!column)
    .flatMap(column => Array.from(column.querySelectorAll('.ghx-issue')));

  issues
    .filter(issue => !!issue)
    .forEach(issue => {
      issue.style.background = getPriorityStyle(issue, defaultStyle);
    });
}

function getPriorityStyle(issue, defaultStyle) {
  const priorityFields = Array.from(issue.querySelectorAll('.ghx-field-icon'))
    .filter(field => field.dataset.tooltip.includes('priority'));

  if (!priorityFields || priorityFields.length == 0) {
    return defaultStyle;
  }

  const priority = priorityFields[0].dataset.tooltip;

  if (priority.includes('Medium')) {
    return 'orange';
  } else if (priority.includes('High')) {
    return 'red';
  } else if (priority.includes('Low')) {
    return 'yellow';
  }
}

highlightColumns();

var pool = document.querySelector('#ghx-pool');

var observer = new MutationObserver(highlightColumns);
observer.observe(pool, { childList: true, attributes: true, subtree: true });

